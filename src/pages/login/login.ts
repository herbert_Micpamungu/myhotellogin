import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
    res:any;
    uname:any;
    pass:any;
    login:any = {};

    constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.res = navParams.get("val");
      this.uname = navParams.get("uname");
      this.pass = this.navParams.get('password');
      }

    logForm(){
      this.uname = this.login['uname'];
      this.pass = this.login['pass'];
      alert("Your username is"+this.uname+" and password is "+ this.pass);
       }
       goHome(){
      this.navCtrl.setRoot(HomePage);
    }
    goRegister(){
   this.navCtrl.setRoot(RegisterPage);
 }

  }
